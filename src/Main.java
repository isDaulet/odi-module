import oracle.odi.core.OdiInstance;
import oracle.odi.core.persistence.IOdiEntityManager;
import oracle.odi.core.persistence.transaction.ITransactionManager;
import oracle.odi.core.persistence.transaction.ITransactionStatus;
import oracle.odi.core.persistence.transaction.support.DefaultTransactionDefinition;
import oracle.odi.domain.mapping.*;
import oracle.odi.domain.project.OdiFolder;
import oracle.odi.domain.project.OdiProject;
import oracle.odi.domain.project.finder.IOdiProjectFinder;
import oracle.odi.publicapi.samples.SimpleOdiInstanceHandle;
import java.io.IOException;
import java.util.Iterator;

/**
 * Adding "ROWS" component and set conditions for split to existing mapping.
 * @author isDaulet (daujlet@yandex.com).
 * @version 0.3.
 * @since 06.05.19.
 */
public class Main {
    public static final String PROJECT = "STAT_REPORTS";
    public static final String FOLDER = "firstFolder";
    public static final String SUBFOLDER = "secondFolder";
    public static final String SUBSUBFOLDER = "thirdFolder";
    public static final String MAPPING_NAME = "testOne";
    public static final String AGGR_COMPONENT = "ROW_1";
    public static final String SPLIT_COMPONENT = "SPLIT1";
    public static final String UNION_COMPONENT = "UNION_ALL";
    public static final String EXP_GROUP_BY = "EXPRESSION.D_HCORG, EXPRESSION.D_DATE";
    public static final String ROW_NAME = "ROW_NUM";
    public static final String OUTPUT_CONNECTOR = "OUTPUT";
    public static final int COMPCOUNT = 4;
    public static final String path = "/Users/daulet/Downloads/Split conditions_1.2.txt";

    public static void main(String[] args) {
        RepoConnection repo = new RepoConnection("jdbc:oracle:thin:@(DESCRIPTION=(ENABLE=BROKEN)(CONNECT_TIMEOUT=5)" +
                "(RETRY_COUNT=3)(TRANSPORT_CONNECT_TIMEOUT=3)(FAILOVER=ON)(ADDRESS_LIST=(ADDRESS=(HOST=db1pdbdev-dc1.mzsr.kz)" +
                "(PORT=1521)(PROTOCOL=tcp))) (ADDRESS_LIST=(ADDRESS=(HOST=db1pdbdev-dc2.mzsr.kz)(PORT=1521)(PROTOCOL=tcp))) " +
                "(CONNECT_DATA=(SERVICE_NAME=ODI_SRVC.mzsr.kz)))",
                "DEV_ODI_REPO");   // Your Master repository URL and username
        SimpleOdiInstanceHandle odiInstanceHandle = SimpleOdiInstanceHandle.create(repo.getUrl(), repo.getDriver(),
                repo.getDbUser(),
                repo.getDbPass(),
                repo.getwRepo(),
                repo.getOdiUser(),
                repo.getOdiPass());
        OdiInstance odiInstance = odiInstanceHandle.getOdiInstance();
        try {
            OdiProject project = ((IOdiProjectFinder) odiInstance.getTransactionalEntityManager().getFinder(OdiProject.class)).findByCode(PROJECT);
            Iterator<OdiFolder> iterator = project.getFolders().iterator();
            Mapping mapping = null;
//            while (iterator.hasNext()) {
//                OdiFolder folder = iterator.next();
//                if ( folder.getName().equals(FOLDER)) {
//                    System.out.println("0. FOLDER -> " + folder.getName());
//                    Iterator<OdiFolder> iterator1 = folder.getSubFolders().iterator();
//                    while (iterator1.hasNext()) {
//                        OdiFolder subFol = iterator1.next();
//                        if (subFol.getName().equals(SUBFOLDER)) {
//                            System.out.println("1. SUBFOLDER --> " + subFol.getName());
//                            for (Mapping map : subFol.getMappings()) {
//                                if (map.getName().equals(MAPPING_NAME)) {
//                                    System.out.println("Mapping name is " + map.getName());
//                                    mapping = map;
//                                    break;
//                                }
//                            }
//                        }
//                    }
//
//                }
//            }

            while (iterator.hasNext()) {
                OdiFolder folder = iterator.next();
                for (Mapping map : folder.getMappings()) {
                    if (map.getName().equals(MAPPING_NAME)) {
                        mapping = map;
                        System.out.println("Mapping name is " + mapping.getName());
                        break;
                    }
                }
            }
            if (mapping != null) {
                FileToList f = new FileToList(path);
                DefaultTransactionDefinition txnDef = new DefaultTransactionDefinition();
                ITransactionManager tm = odiInstance.getTransactionManager();
                ITransactionStatus txnStatus = tm.getTransaction(txnDef);
                IOdiEntityManager entityManager = odiInstance.getTransactionalEntityManager();
                f.addSplitOutput(entityManager, mapping);
//                f.addRows(entityManager, mapping);
//                f.setConditions(entityManager, mapping);
                tm.commit(txnStatus);
                System.out.println("All is DONE!!!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            odiInstanceHandle.release();
        }
    }
}
