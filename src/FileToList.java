import oracle.odi.core.OdiInstance;
import oracle.odi.core.persistence.IOdiEntityManager;
import oracle.odi.core.persistence.transaction.ITransactionManager;
import oracle.odi.core.persistence.transaction.ITransactionStatus;
import oracle.odi.core.persistence.transaction.support.DefaultTransactionDefinition;
import oracle.odi.domain.mapping.*;
import oracle.odi.domain.mapping.component.AggregateComponent;
import oracle.odi.domain.mapping.component.SetComponent;
import oracle.odi.domain.mapping.component.SplitterComponent;
import oracle.odi.domain.mapping.exception.MapComponentException;
import oracle.odi.domain.mapping.exception.MappingException;
import oracle.odi.domain.mapping.properties.PropertyException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class FileToList {
    private List<String> arr;
    private String FILEPATH;

    public FileToList(String path) throws IOException {
        this.FILEPATH = path;
        this.arr = Files.readAllLines(Paths.get(FILEPATH));
    }

    public void printAll() throws IOException {
        int i = 1;
        for (String str : arr) {
            System.out.println(i + "----> " + str);
            i++;
        }
    }

    //    public void addRowAndSetConditions(OdiInstance odiInstance, Mapping mapping) {
//        try {
//            IMapComponent row1 = mapping.findComponent(Main.AGGR_COMPONENT);
//            SplitterComponent split = (SplitterComponent) mapping.findComponent(Main.SPLIT_COMPONENT);
//            SetComponent union = (SetComponent) mapping.findComponent(Main.UNION_COMPONENT);
//
//            DefaultTransactionDefinition txnDef = new DefaultTransactionDefinition();
//            ITransactionManager tm = odiInstance.getTransactionManager();
//            ITransactionStatus txnStatus = tm.getTransaction(txnDef);
//            IOdiEntityManager entityManager = odiInstance.getTransactionalEntityManager();
//
//            for (int i = 1; i < Main.COMPCOUNT + 1; i++) {
//                if (i < 3) {
//                    AggregateComponent row = (AggregateComponent) mapping.createComponent(row1.getComponentTypeName(), "ROW_" + i);
//                    row.setPropertyValue("MANUAL GROUP BY CLAUSE", Main.EXP_GROUP_BY);
//
//                    MapConnectorPoint connectorPoint = split.getOutputConnectorPoint(Main.OUTPUT_CONNECTOR + i);
//                    split.setSplitterCondition(connectorPoint, arr.get(i - 1));
//                    connectorPoint.connectTo(row);
//                    MapConnector connector = row.connectTo(union);
//
//                    for (MapAttribute attr : row1.getAttributes()) {
//                        MapAttribute attrNew = row.addAttribute(attr.getName(), (attr.getName().equals(Main.ROW_NAME) ? Integer.toString(i) : attr.getExpression().getText()), attr.getDataType(), attr.getSize(), attr.getScale());
//                        union.addSetExpression(attrNew.getName(), "ROW_" + i + "." + attrNew.getName(), connector.getEndPoint());
//                    }
//
//                    //write to repository
//                    entityManager.merge(mapping);
//                    System.out.println("split condition and row - " + i + " is added.");
//                } else {
//                    //create aggr component
//                    AggregateComponent row = (AggregateComponent) mapping.createComponent(row1.getComponentTypeName(), "ROW_" + i);
//                    row.setPropertyValue("MANUAL GROUP BY CLAUSE", Main.EXP_GROUP_BY);
//
//                    MapConnectorPoint connectorPoint = split.createOutputConnectorPoint(Main.OUTPUT_CONNECTOR + i);
//                    split.setSplitterCondition(connectorPoint, arr.get(i - 1));
//                    connectorPoint.connectTo(row);
//                    MapConnector connector = row.connectTo(union);
//
//                    //create attributes
//                    for (MapAttribute attr : row1.getAttributes()) {
//                        MapAttribute attrNew = row.addAttribute(attr.getName(), (attr.getName().equals(Main.ROW_NAME) ? Integer.toString(i) : attr.getExpression().getText()), attr.getDataType(), attr.getSize(), attr.getScale());
//                        union.addSetExpression(attrNew.getName(), "ROW_" + i + "." + attrNew.getName(), connector.getEndPoint());
//                    }
//
//                    //write to repository
//                    entityManager.merge(mapping);
//                    System.out.println("split condition and row - " + i + " is added.");
//                }
//            }
//            tm.commit(txnStatus);
//        } catch (MappingException e) {
//            e.printStackTrace();
//        }
//    }
    public List<String> getConditions(OdiInstance odiInstance, Mapping mapping) {
        List<String> list = new ArrayList<>();
        try {
            SplitterComponent split = (SplitterComponent) mapping.findComponent(Main.SPLIT_COMPONENT);

            DefaultTransactionDefinition txnDef = new DefaultTransactionDefinition();
            ITransactionManager tm = odiInstance.getTransactionManager();
            ITransactionStatus txnStatus = tm.getTransaction(txnDef);
            IOdiEntityManager entityManager = odiInstance.getTransactionalEntityManager();

            for (int i = 1; i < Main.COMPCOUNT; i++) {
                MapConnectorPoint connectorPoint = split.getOutputConnectorPoint(Main.OUTPUT_CONNECTOR + Integer.toString(i));
                list.add(split.getSplitterCondition(connectorPoint).getText());

                //write to repository
                entityManager.merge(mapping);
                System.out.println("split condition - " + i + " is readed.");
            }
            tm.commit(txnStatus);
        } catch (MapComponentException | PropertyException e) {
            e.printStackTrace();
        }
        return list;
    }
    public void setConditions(IOdiEntityManager entityManager, Mapping mapping) {
        try {
            SplitterComponent split = (SplitterComponent) mapping.findComponent(Main.SPLIT_COMPONENT);

            for (int i = 1; i < Main.COMPCOUNT + 1; i++) {
                if (i < 3) {
                    MapConnectorPoint connectorPoint = split.getOutputConnectorPoint(Main.OUTPUT_CONNECTOR + i);
                    split.setSplitterCondition(connectorPoint, arr.get(i - 1));

                    //write to repository
                    entityManager.merge(mapping);
                    System.out.println("split condition - " + i + " is set.");
                } else {
                    MapConnectorPoint connectorPoint = split.createOutputConnectorPoint(Main.OUTPUT_CONNECTOR + i);
                    split.setSplitterCondition(connectorPoint, arr.get(i - 1));

                    //write to repository
                    entityManager.merge(mapping);
                    System.out.println("split condition - " + i + " is set.");
                }
            }
//            tm.commit(txnStatus);
        } catch (MappingException e) {
            e.printStackTrace();
        }
    }
    public void addRows(IOdiEntityManager entityManager, Mapping mapping) {
        try {
            IMapComponent row1 = mapping.findComponent(Main.AGGR_COMPONENT);
            SplitterComponent split = (SplitterComponent) mapping.findComponent(Main.SPLIT_COMPONENT);
            SetComponent union = (SetComponent) mapping.findComponent(Main.UNION_COMPONENT);

            for (int i = 2; i < Main.COMPCOUNT + 1; i++) {

                //create aggr component
                AggregateComponent row = (AggregateComponent) mapping.createComponent(row1.getComponentTypeName(), "ROW_" + i);
                row.setPropertyValue("MANUAL GROUP BY CLAUSE", Main.EXP_GROUP_BY);
                MapConnectorPoint connectorPoint = split.getOutputConnectorPoint(Main.OUTPUT_CONNECTOR + i);
                connectorPoint.connectTo(row);
                MapConnector connector = row.connectTo(union);

                //create attributes
                for (MapAttribute attr : row1.getAttributes()) {
                    MapAttribute attrNew = row.addAttribute(attr.getName(), (attr.getName().equals(Main.ROW_NAME) ? Integer.toString(i) : attr.getExpression().getText()), attr.getDataType(), attr.getSize(), attr.getScale());
                    union.addSetExpression(attrNew.getName(), "ROW_" + i + "." + attrNew.getName(), connector.getEndPoint());
                }

                //write to repository
                entityManager.merge(mapping);
                System.out.println("row - " + i + " is added.");
            }
//            tm.commit(txnStatus);
        } catch (MappingException e) {
            e.printStackTrace();
        }
    }
    public void addSplitOutput(IOdiEntityManager entityManager, Mapping mapping) {
        try {
            SplitterComponent split = (SplitterComponent) mapping.findComponent(Main.SPLIT_COMPONENT);
            for (int i = 3; i < Main.COMPCOUNT + 1; i++) {
                split.createOutputConnectorPoint(Main.OUTPUT_CONNECTOR + i);
                entityManager.merge(mapping);
                System.out.println("split condition - " + i + " is added.");
            }
        } catch (MapComponentException e) {
            e.printStackTrace();
        } catch (MappingException e) {
            e.printStackTrace();
        }

    }
}
