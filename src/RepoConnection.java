public class RepoConnection {
    private String url;  //URL
    private String driver = "oracle.jdbc.OracleDriver";   //Driver
    private String dbUser;   //Your Master repository username
    private char[] dbPass = new String("bae2ooWi").toCharArray();   //Your Master repository password
    private String wRepo = "WORKREP";    //Work repository name
    private String odiUser = "SUPERVISOR";  //ODI login username
    private char[] odiPass = new String("bae2ooWi").toCharArray();  //ODI login password

    public RepoConnection(String url, String dbUser) {
        this.url = url;
        this.dbUser = dbUser;
    }
    public RepoConnection(String url, String driver, String dbUser, String dbPass, String wRepo, String odiUser, String odiPass) {
        this.url = url;
        this.driver = driver;
        this.dbUser = dbUser;
        this.dbPass = dbPass.toCharArray();
        this.wRepo = wRepo;
        this.odiUser = odiUser;
        this.odiPass = odiPass.toCharArray();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public char[] getDbPass() {
        return dbPass;
    }

    public void setDbPass(char[] dbPass) {
        this.dbPass = dbPass;
    }

    public String getwRepo() {
        return wRepo;
    }

    public void setwRepo(String wRepo) {
        this.wRepo = wRepo;
    }

    public String getOdiUser() {
        return odiUser;
    }

    public void setOdiUser(String odiUser) {
        this.odiUser = odiUser;
    }

    public char[] getOdiPass() {
        return odiPass;
    }

    public void setOdiPass(char[] odiPass) {
        this.odiPass = odiPass;
    }
}
